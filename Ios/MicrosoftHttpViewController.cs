using System;
using MonoTouch.UIKit;
using System.Drawing;
using IosWebRequest;
using ModernHttpClient;
using System.Net.Http;

namespace IosMicrosoftHttp
{
    public class MicrosoftHttpViewController : BaseViewController
    {
        public MicrosoftHttpViewController() 
        {
            NetworkWorker.Handler = new AFNetworkHandler("http://127.0.0.1");
            var plain = NetworkWorker.Handler as HttpClientHandler;
            if(plain != null)
                plain.Proxy = System.Net.WebRequest.GetSystemWebProxy();
        }
    }
}

