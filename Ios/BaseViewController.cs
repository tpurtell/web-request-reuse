using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MonoTouch.UIKit;
using System.Drawing;

namespace IosWebRequest
{
    public class BaseViewController : UIViewController
    {
        private UIButton _Add;
        private UIButton _Remove;
        private UILabel _Connections;
        private UILabel _Requests;
        const float BUTTON_HEIGHT = 50;

        public BaseViewController()
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            View.Frame = UIScreen.MainScreen.Bounds;
            View.BackgroundColor = UIColor.White;
            View.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;

            var worker = new NetworkWorker();

            var top = 0f;
            _Add = UIButton.FromType(UIButtonType.RoundedRect);
            _Add.Frame = new RectangleF(0, top, View.Frame.Width, BUTTON_HEIGHT);
            _Add.SetTitle("Start a network worker", UIControlState.Normal);
            _Add.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            _Add.TouchUpInside += (sender, args) => worker.StartOne();
            View.AddSubview(_Add);
            
            top += BUTTON_HEIGHT;
            _Remove = UIButton.FromType(UIButtonType.RoundedRect);
            _Remove.Frame = new RectangleF(0, top, View.Frame.Width, BUTTON_HEIGHT);
            _Remove.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            _Remove.SetTitle("Stop a network worker", UIControlState.Normal);
            _Remove.TouchUpInside += (sender, args) => worker.StopOne();
            View.AddSubview(_Remove);

            top += BUTTON_HEIGHT;
            _Connections = new UILabel();
            _Connections.Frame = new RectangleF(0, top, View.Frame.Width, BUTTON_HEIGHT);
            _Connections.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            View.AddSubview(_Connections);
            
            top += BUTTON_HEIGHT;
            _Requests = new UILabel();
            _Requests.Frame = new RectangleF(0, top, View.Frame.Width, BUTTON_HEIGHT);
            _Requests.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            View.Add(_Requests);

            var scheduler = TaskScheduler.FromCurrentSynchronizationContext();
            new Timer(state =>
            {
                Task.Factory.StartNew(() =>
                {
                    _Connections.Text = string.Format("{0} connections", worker.ConnectionCount);
                    _Requests.Text = string.Format("{0} requests", worker.RequestCount);
                }, CancellationToken.None, TaskCreationOptions.None, scheduler);
            }, null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));

        }

    }
}

