﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Widget;
using Android.OS;
using ModernHttpClient;

namespace AndroidTest
{
    [Activity(Label = "AndroidMicrosoftHttp", MainLauncher = true)]
    public class MicrosoftHttpActivity : BaseActivity
    {
        public MicrosoftHttpActivity() 
        {
            NetworkWorker.Handler = new OkHttpNetworkHandler();
            var plain = NetworkWorker.Handler as HttpClientHandler;
            if(plain != null)
                plain.Proxy = System.Net.WebRequest.GetSystemWebProxy();
        }
    }
}

