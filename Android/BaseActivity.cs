﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Widget;
using Android.OS;

namespace AndroidTest
{
    public class BaseActivity : Activity
    {
        private Button _Add;
        private Button _Remove;
        private TextView _Connections;
        private TextView _Requests;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);

            var worker = new NetworkWorker();

            _Add = FindViewById<Button>(Resource.Id.AddButton);
            _Remove = FindViewById<Button>(Resource.Id.RemoveButton);

            _Add.Click += (sender, args) => worker.StartOne();
            _Remove.Click += (sender, args) => worker.StopOne();

            var scheduler = TaskScheduler.FromCurrentSynchronizationContext();
            _Connections = FindViewById<TextView>(Resource.Id.Connections);
            _Requests = FindViewById<TextView>(Resource.Id.Requests);
            new Timer(state =>
            {
                Task.Factory.StartNew(() =>
                {
                    _Connections.Text = string.Format("{0} connections", worker.ConnectionCount);
                    _Requests.Text = string.Format("{0} requests", worker.RequestCount);
                }, CancellationToken.None, TaskCreationOptions.None, scheduler);
            }, null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));

        }

    }
}

