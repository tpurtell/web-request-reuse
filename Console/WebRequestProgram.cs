﻿using System;
using System.Net;
using System.Threading;

namespace ConsoleWebRequest
{
    class Program
    {
        static void Main(string[] args)
        {
            var worker = new NetworkWorker();

            worker.StartOne();

            for (;;)
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));
                Console.WriteLine("Total requests {0} Current connections {1}:", worker.RequestCount, worker.ConnectionCount);
            }
        }
    }
}
