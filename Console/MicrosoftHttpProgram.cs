﻿using System;
using System.Net;
using System.Threading;
using System.Net.Http;

namespace ConsoleWebRequest
{
    partial class Program
    {
        static void Main(string[] args)
        {
            var worker = new NetworkWorker();

            var plain = NetworkWorker.Handler as HttpClientHandler;
            if(plain != null)
                plain.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            worker.StartOne();

            for (;;)
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));
                Console.WriteLine("Total requests {0} Current connections {1}:", worker.RequestCount, worker.ConnectionCount);
            }
        }
    }
}
