var http = require('http');
var crypto = require('crypto')

http.createServer(function (req, res) {
  var len = Math.ceil(Math.random() * 1500 + 150) * 10;
  console.log("req: " + len + " port: " + req.socket.remotePort);
  res.writeHead(200, {'Content-Type': 'application/octet-stream', 'Content-Length':"" + len});
  var buf = crypto.randomBytes(len / 10);
  var count = 10;
  var interval = setInterval(function() {
	if (--count == 0) {
	  res.end(buf);
	  clearInterval(interval);
	  return;
	}
	res.write(buf);
  },Math.random() * 10);
}).listen(9615);