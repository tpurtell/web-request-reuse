using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

public class NetworkWorker : BaseWorker
{
    public static HttpClient Client {get; private set;}
    private static HttpMessageHandler _Handler;
    public static HttpMessageHandler Handler
    {
        get 
        {
            if(_Handler == null) {
                _Handler = new HttpClientHandler();
                Client = new HttpClient(_Handler);
            }
            return _Handler;
        }
        set 
        {
            _Handler = value;
            Client = new HttpClient(_Handler);
        }
    }
    public NetworkWorker()
    {
        var handler = Handler;
    }
    protected override string DoRequest(CancellationToken token)
    {
        try
        {
            using (var request = new HttpRequestMessage(HttpMethod.Get, TEST_URI))
            {
                using (var reply = Client.SendAsync(request).Result)
                {
                    reply.EnsureSuccessStatusCode();
                    using (var reader = new StreamReader(reply.Content.ReadAsStreamAsync().Result))
                        return reader.ReadToEnd();
                }
            }
        }
        catch (Exception e)
        {
            return null;
        }
    }
}