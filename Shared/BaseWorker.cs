using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

public abstract class BaseWorker
{
    public static readonly Uri TEST_URI = new Uri("http://192.168.1.128:9615");
    private List<CancellationTokenSource> _CancellationTokens = new List<CancellationTokenSource>();
    public int RequestCount { get; private set; }

    public int ConnectionCount { get; private set; }
    public BaseWorker()
    {
    }

    public void StartOne()
    {
        var token = new CancellationTokenSource();
        lock (_CancellationTokens)
            _CancellationTokens.Add(token);
        Task.Factory.StartNew(() => Worker(token.Token), token.Token);
    }
    private static List<int> _Ports = new List<int>();
    private void Worker(CancellationToken token)
    {
        for (;;)
        {
            token.ThrowIfCancellationRequested();
            lock (_CancellationTokens)
                ++RequestCount;
            var resp = DoRequest(token);
            try 
            {
                var port = int.Parse(resp);
                lock(_Ports) 
                {
                    if(!_Ports.Contains(port)) 
                    {
                        ConnectionCount++;
                        _Ports.Add(port);
                    }
                    //just because if it really goes up to 1000, then all of those
                    //must have been closed by now
                    if(_Ports.Count > 1000)
                        _Ports.Clear();
                }
            } 
            catch(Exception e) 
            {
            }
        }
    }

    protected abstract string DoRequest(CancellationToken token);


    public int NumWorkers
    {
        get
        {
            lock (_CancellationTokens)
                return _CancellationTokens.Count;
        }
    }
    public void StopOne()
    {
        lock (_CancellationTokens)
        {
            if (_CancellationTokens.Count == 0)
                return;
            var index = new Random().Next(_CancellationTokens.Count);
            var token = _CancellationTokens[index];
            _CancellationTokens.RemoveAt(index);
            token.Cancel();
        }
    }
}