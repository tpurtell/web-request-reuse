using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

public class NetworkWorker : BaseWorker
{
    public NetworkWorker()
    {

    }
    public WeakReference LastServicePoint = new WeakReference(null);

    protected override string DoRequest(CancellationToken token)
    {
        try
        {
            var request = WebRequest.Create(TEST_URI) as HttpWebRequest;
            using (var reply = request.GetResponse())
            {
                using (var response = new StreamReader(reply.GetResponseStream()))
                    return response.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            return null;
        }
    }
}